﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiColumnListView_MVVM.MVVM.Model
{
    public class Model
    {
        public string FullName { get; set; }
        public string LastName { get; set; }
        public string DateofBirth { get; set; }
        public Model(string fullName, string lastname, string dob)
        {
            this.LastName = lastname;
            this.FullName = fullName;
            this.DateofBirth = dob;
        }
    }
}
